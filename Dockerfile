FROM postgres:10.4-alpine

RUN mkdir -p /opt/backup
COPY bkp-db.sh /opt/backup
WORKDIR /opt/backup

RUN apk add dos2unix --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/community/ --allow-untrusted

RUN dos2unix ./bkp-db.sh

#ENTRYPOINT ["./bkp-db.sh"]

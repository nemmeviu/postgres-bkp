#!/bin/sh
#
#echo "$HOST_DB:$PORT_DB:$POSTGRES_DB:$USER_DB:$PASSWORD_DB" > ~/.pgpass

set -x

[ -z "$APP_NAME" ] && exit 0

DST_DIR="/var/backup/$APP_NAME/"
if [ ! -d "$DST_DIR" ]; then
    mkdir -p "$DST_DIR"
fi

chown postgres "$DST_DIR"
pg_dump -U $USER_DB -h $HOST_DB -d $POSTGRES_DB > $DST_DIR/$POSTGRES_DB-$(date +%m-%d-%y).sql

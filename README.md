# postgres-bkp

simple script to create a postgres database dump

## ENV Variables

For inventory-manager, is necessary to understand two definitions of env's:

#### ENV for Publisher (PUB)

| var name    | default value  | required | description |
|:-----------:|:--------------:|:--------:|:-----------:|
| APP_NAME    |                | X        | folder name inside /var/backup/ volume |
| USER_DB     |                | X        | Postgres DB User |
| POSTGRES_DB |                | X        | Postgres DB Name |
| HOST_DB     |                | X        | Hostname of DB |

### Usage

```
$ docker run -v /tmp/bkp/:/var/backup --rm \
    --network=host -e HOST_DB=localhost \
    -e POSTGRES_DB=my_database -e USER_DB=my_user \
    -e DB_PASS=my_pass -e APP_NAME=my_app \
    blacktourmaline/postgres-bkp:10.4-alpine
$
$ ls /tmp/bkp/my_app/
my_database-12-05-18.sql
```

